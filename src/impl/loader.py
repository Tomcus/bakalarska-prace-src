import io, os, sys, types
from IPython import get_ipython
from nbformat import read
import re
from IPython.core.interactiveshell import InteractiveShell

def find_notebook(fullname, path=None):
    """find a notebook, given its fully qualified name and an optional path

    This turns "foo.bar" into "foo/bar.ipynb"
    and tries turning "Foo_Bar" into "Foo Bar" if Foo_Bar
    does not exist.
    """
    name = fullname.rsplit('.', 1)[-1]
    if path is None:
        path = ['']
    for d in path:
        nb_path = os.path.join(d, name + ".ipynb")
        if os.path.isfile(nb_path):
            return nb_path
        # let import Notebook_Name find "Notebook Name.ipynb"
        nb_path = nb_path.replace("_", " ")
        if os.path.isfile(nb_path):
            return nb_path

        
class NotebookLoader(object):
    """Module Loader for Jupyter Notebooks"""
    def __init__(self, path=None):
        self.shell = InteractiveShell.instance()
        self.path = path
    
    def load_module(self, fullname):
        """import a notebook as a module"""
        path = find_notebook(fullname, self.path)

        print ("importing Jupyter notebook from %s" % path)

        # load the notebook object
        print(path)
        with io.open(path, 'r', encoding='utf-8') as f:
            nb = read(f, 4)
        res = re.search(r'^(?P<file_name>.*)\.ipynb$', path)
        new_py_file = None
        if res:
            new_py_file = res.group('file_name') + '.py'
            print("Creating new python file: {}".format(new_py_file))
        
        new_py = None
        if new_py_file:
            new_py = open(new_py_file, 'w')
        
        # create the module and add it to sys.modules
        # if name in sys.modules:
        #    return sys.modules[name]
        mod = types.ModuleType(fullname)
        mod.__file__ = path
        mod.__loader__ = self
        mod.__dict__['get_ipython'] = get_ipython
        sys.modules[fullname] = mod

        # extra work to ensure that magics that would affect the user_ns
        # actually affect the notebook module's ns
        save_user_ns = self.shell.user_ns
        self.shell.user_ns = mod.__dict__

        try:
            for cell in nb.cells:
#                 print(cell.metadata)
                if cell.cell_type == "code" and cell.metadata is not None and "tags" in cell.metadata and "importable" in cell.metadata.tags:
                    # transform the input to executable Python
                    code = self.shell.input_transformer_manager.transform_cell(cell.source)
                    if new_py is not None:
#                        print(code)
                        new_py.write(code)
                    # run the code in themodule
                    exec(code, mod.__dict__)
        finally:
            self.shell.user_ns = save_user_ns
            
        if new_py is not None:
            spark.sparkContext.addFile('./' + new_py_file)
        return mod

class NotebookFinder(object):
    """Module finder that locates Jupyter Notebooks"""
    def __init__(self):
        self.loaders = {}

    def find_module(self, fullname, path=None):
        nb_path = find_notebook(fullname, path)
        if not nb_path:
            return

        key = path
        if path:
            # lists aren't hashable
            key = os.path.sep.join(path)

        if key not in self.loaders:
            self.loaders[key] = NotebookLoader(path)
        return self.loaders[key]

# Add NotebookFinder to system
sys.meta_path.append(NotebookFinder())

# Add all *.py files to spark context so function from those python files can be used in spark queries
for file_name in os.listdir('.'):
    if re.match('^.*\.py$', file_name) and file_name != __file__:
        print('Adding {} to spark context'.format(file_name))
        spark.sparkContext.addFile(file_name)
