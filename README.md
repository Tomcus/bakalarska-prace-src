# Bakalářská práce

Zde lze najít veškeré dokumenty k mé bakalářské práci.

## Složka text

Tato složka obsahuje výsledný text bakalářské práce, který byl i zároveň vytištěn.

## Složka src

Tato složka obsahuje 2 podloky

### Složka impl

Tato složka obsahuje veškeré zdrojové kódy vytvořené k této práci. Bohužel pokud uživatel nemá přístup do interní sítě firmy Avast a oprávnění spouštět kód v systému Burger, tak není možné tento kód spustit.

### Složka thesis

Obashuje zdrojové kódy, které bylo vytořeny pro samotný text práce.
Zároveň lze zde najít zdrojové obrázky, ze kterých jsem vytvořil PDF soubory.
